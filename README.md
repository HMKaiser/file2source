# file2source
A graphical tool to create C arrays from files or RAW images.

An easy way to include binary data to your source code.
It can also convert RGB RAW pictures to RGBA format with selection of alpha color.
